$( document ).ready(function() {

    $('.sm-plugin-img-container').smShowImageOnFileSelect();
    
});

;(function( $ ) {

    $.fn.smShowImageOnFileSelect = function(options) {

        var originalImage = '';
        var defaults = {
            imageHolder: '.sm-plugin-img-container'

        };

        var settings = $.extend( {}, defaults, options );


        originalImage = $(settings.imageHolder +' img').attr('src');
        $(settings.imageHolder).on('change', 'input[type="file"]', function(){

        if (this.files && this.files[0]) {

                    var reader = new FileReader();

                    reader.onload = function (e) {

                        var image = new Image();
                        image.src = e.target.result;

                         image.onload = function() {

                            $(settings.imageHolder  +' img').attr('src', e.target.result);
                            //if(this.width < $(settings.imageHolder).width()){
                                setImageDimensions(this.width, $(settings.imageHolder).width(), settings.imageHolder);
                            //}
                         }

                    }

                    reader.readAsDataURL(this.files[0]);

                }else{

                if(originalImage != undefined){
                    $(settings.imageHolder +' img').attr('src', originalImage);
                }

                }
        });

        return this;
    };


    function realImgDimension(img) {
        var i = new Image();
        i.src = img.src;
        return {
            naturalWidth: i.width,
            naturalHeight: i.height
        };
    }


    function setImageDimensions(imgWidth, containerWidth, container){

        if(imgWidth < containerWidth){
            $(container + ' img').attr('width', 'auto')
        }else{
            $(container + ' img').attr('width', '100%')
        }
    }


}( jQuery ));